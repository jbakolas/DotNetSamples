DotNetSamples
=============

A collection of very very very basic .NET and C# samples. You can browse and see various usage of 

C# Arguments
Chat with TCP
Collections
Delegates
DirectorySearcher
Events
Exceptions
Extension Methods
Fibonacci
FileReaderWriter
FileSystemWatcher
Generics and IO
Globalization
HttpWebRequest
MD5 Hashing
Object Serialization
Processes enumeration
Reflection
Regex and pattern matching
SendMail
StopWatch
StringBuilder
Threading
Timers
XML DOM
